
# Basic API

## task
Write an API endpoint that returns a filtered set of organizations from the data provided:
https://s3-us-west-2.amazonaws.com/sample-coding-dataset/organization_sample_data.csv
Example API: GET /organizations?category=Greek&city=Washington
Fields: Id: numeric id Name: string //organization name City: string //US city name State: string //US state
name Postal: string //US postal code Category: string //categorization of org
Additional query params: Orderby: string //fieldname to order the results by Direction: string //ASC or
DSC
The expected response is a JSON collection of organizations:
{
"organizations": [
{
"id":"102", "name":"Sigma Kappa Zeta Chapter", "city":"Washington", "state":"DC", "postal":"20052",
"category":"Greek" }, ... ] }
All query parameters are optional.

At a minimum: - Your API endpoint URL is /organizations - Your API should correctly filter any
combination of API parameters - Results should be properly ordered if the orderby parameter is included

## Quick start
run `run_server.sh` and navigate to http://localhost:5000/apidocs/ for swagerio interface.

## Run tests
run `run_tests.sh` and coverage will be shown in the console

## Assumptions and codeing choices
Due to the small dataset, and small scope of this project the following is true:
 - all records will be loaded on app start and use sqlite. In production this would be replaced with dedicated sql servers that could load balance.
 - a single file flask app was used for simplicity (Blueprints format would be used for larger app)
 - Minimal unit tests exist to show the idea, near 99-100% coverage would be expected for production
 - No Negative testing, this would be expected in production
 - minimal swagger. I would expect error objects/responses to be mapped out.
 - 

