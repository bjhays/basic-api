#!/bin/bash -ex
test_dir="$(dirname $0)/project/tests"

if [ ! -d venv ]; then
    virtualenv venv
fi

. venv/bin/activate

pip install -r ${test_dir}/requirements.txt
pytest -rsvvvv --cov-config=.coveragerc --cov-report term-missing --cov=project ${test_dir}
