#!/bin/bash
compose="$(which docker-compose)"
composer="docker/docker-compose.yml"
docker_ps="$(docker ps 2>&1)"


if [[ "${docker_ps}" == *"Cannot connect to the Docker daemon"* ]]; then
    echo "docker is not started or can't be reached, please start and try again"
    exit 1
fi

echo "Using docker-compose '${compose}'"
if [ -z "${compose}" ]; then
    echo "docker-compose not found."
    exit 1
fi

function do_docker() {
    docker-compose -f ${composer} $1
}

do_docker config
do_docker "up --build" 
# do_docker up 

trap 'do_docker down' EXIT
