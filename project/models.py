from sqlalchemy import Column, Integer, String

from .database import Base


class Organizations(Base):
    __tablename__ = 'organizations'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    city = Column(String(50))
    state = Column(String(50))
    postal = Column(Integer())
    category = Column(String(50))

    def __init__(self, name, city, state, postal, category):
        self.name = name
        self.city = city
        self.state = state
        self.postal = postal
        self.category = category

    def __repr__(self):
        return '<Organization %r>' % (self.name)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
