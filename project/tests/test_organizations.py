from project import app
from project.models import Organizations
import unittest
import mock


test_data = [Organizations(*{"name": "Sigma Kappa Zeta Chapter",
                             "city": "Washington",
                             "state": "DC",
                             "postal": "20052",
                             "category": "Greek"})]


class TestOrganizations(unittest.TestCase):

    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def tearDown(self):
        pass

    def test_root_endpoint(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    @mock.patch('project.do_search', return_value=test_data)
    def test_organizations_endpoint(self, mocked_do_search):
        response = self.app.get('/organizations', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        print(response.json)
        orgs = []
        for o in test_data:
            orgs.append(o.as_dict())
        res = {'organizations': orgs}
        assert response.json == res

    def test_organization_endpoint_exception(self):
        with mock.patch('project.do_search') as do_search_mock:
            do_search_mock.side_effect = Exception("Boom")
            response = self.app.get('/organizations', follow_redirects=True)
            self.assertEqual(response.status_code, 500)


if __name__ == '__main__':
    unittest.main()
