from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

engine = create_engine('sqlite:////tmp/test.db', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db(app):
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    from project.models import Organizations  # noqa
    from os import path # noqa
    import csv  # noqa
    from time import time  # noqa

    if path.exists("/tmp/test.db"):
        app.logger.info("DB Exists")
    else:
        app.logger.info("DB doesn't exist")

        Base.metadata.drop_all(bind=engine)
        Base.metadata.create_all(bind=engine)

        app.logger.info(Organizations.query.all())
        t = time()
        s = db_session

        with open('organization_sample_data.csv', mode='rU') as f:
            reader = csv.reader(f, skipinitialspace=True, delimiter=',')
            header = None
            row = None
            try:
                for row in reader:
                    if header is None:
                        header = row
                        continue
                    app.logger.info(row)
                    del(row[0])
                    row[0] = row[0].decode('utf8').strip()
                    org = Organizations(*row)
                    s.add(org)  # Add all the records

                s.commit()  # Attempt to commit all the records
            except:
                app.logger.info("Failed to load '%s'" % row)
                s.rollback()  # Rollback the changes on error
                raise
            finally:
                s.close()  # Close the connection

            app.logger.info("%s records initialized in %ss" % (Organizations.query.count(), str(time() - t)))
