from flask import Flask, jsonify, request
from sqlalchemy import text
from project.models import Organizations
from flasgger import Swagger

app = Flask(__name__)

swagger_config = {
    "headers": [
    ],
    "specs": [
        {
            "endpoint": 'spec',
            "route": '/spec.json',
            "rule_filter": lambda rule: True,  # all in
            "model_filter": lambda tag: True,  # all in
        }
    ],
    "static_url_path": "/flasgger_static",
    "swagger_ui": True,
    "specs_route": "/apidocs/"
}

swagger = Swagger(app, config=swagger_config)


@app.route('/organizations')
def organizations():
    """Rest endpoint to return organization data.

    ---
    parameters:
      - name: id
        description: organization id number
        in: query
        type: int

      - name: name
        description: organization name (exact match)
        in: query
        type: string

      - name: city
        description: organization city (exact match)
        in: query
        type: string

      - name: state
        description: organization state (abreviation)
        in: query
        type: string
        enum: ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA",
          "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
          "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
          "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
          "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]

      - name: postal
        description: organization 7-digit postal code
        in: query
        type: string

      - name: category
        description: organization category
        in: query
        type: string
        enum: ["Civic", "Community", "Education", "For-Profit", "Greek", "Government", "Non-Profit"]

      - name: orderby
        description: result order direction
        in: query
        type: string
        enum: ['asc', 'desc']
        default: desc

      - name: orderby
        description: result order column
        in: query
        type: string
        enum: ["id", "name", "city", "state", "postal", "category"] 
        default: id

      - name: limit
        description: number of results to return
        in: query
        type: int
        default: 10

      - name: page
        description: page of results to return
        in: query
        type: int
        default: 0


    definitions:
      Organizations:
        type: array
        items:
          $ref: '#/definitions/Organization'
      Organization:
        type: object
        properties:
          category:
            type: string
          city:
            type: string
          id:
            type: int
          name:
            type: string
          postal:
            type: int
          state:
            type: string

    responses:
      200:
        description: A list organizations (may be filtered)
        schema:
          $ref: '#/definitions/Organizations'
    """
    orderby = request.args.get('orderby', default="id")
    direction = request.args.get('direction', default="desc")
    page = request.args.get('page', default=0)
    limit = request.args.get('limit', default=10)
    debug = (request.args.get('debug', default="false").lower() == "true")

    filters = {"id": request.args.get('id'),
               "name": request.args.get('name'),
               "city": request.args.get('city'),
               "state": request.args.get('state'),
               "postal": request.args.get('postal'),
               "category": request.args.get('category')}

    try:
        app.logger.debug("do_search")
        res = do_search(filters, orderby, direction, page, limit)
    except Exception as e:
        app.logger.error(e, exc_info=True)
        return "There was an error, please see logs for details.", 500

    ret = {"organizations": []}
    if debug:
        ret["_debug"] = {"limit": limit, "page": page, "filters": filters}
    for o in res:
        ret["organizations"].append(o.as_dict())
    app.logger.debug(ret)
    return jsonify(ret)


# catch all
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return '%s is not available' % path, 400


def do_search(filters={}, orderby="id", direction="desc", page=0, limit=10):
    """Search the database for requested org info."""
    qry = Organizations.query

    for f in filters:
        if filters[f]:
            qry = qry.filter(Organizations.__table__.c[f] == filters[f])

    return qry.order_by(text("%s %s" % (orderby.lower(), direction.lower()))).limit(limit).offset(page).all()
    # .filter(Organizations.name == 'Michigan Science Center')
