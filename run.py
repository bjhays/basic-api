from os import getenv
from project import app
from project.database import db_session, init_db

FLASK_DEBUG = (getenv('FLASK_DEBUG').lower() == 'true')

CONFIG_ENV = getenv('CONFIG_ENV', 'dev').lower()
app.config.from_pyfile("config/%s.cfg" % CONFIG_ENV)


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

if __name__ == "__main__":
    init_db(app)
    app.run(host="0.0.0.0", debug=FLASK_DEBUG) #, use_reloader=False)
