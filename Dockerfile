FROM python:2.7
ADD . /project
WORKDIR /project
RUN pip install -r /project/requirements.txt
CMD python /project/run.py
